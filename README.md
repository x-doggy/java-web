﻿# Веб-кодирование

## На языке Java

| № | Ссылка | Инструкция |
|---|--------|------------|
| 1 | [XHelloWeb](XHelloWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps_ru.html) |
| 2 | [XStrutsWeb](XStrutsWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps-struts_ru.html) |
| 3 | [XMySQLWeb](XMySQLWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/mysql-webapp_ru.html) |
| 4 | [XServletListenerWeb](XServletListenerWeb/) | [viralpatel.net](http://viralpatel.net/blogs/jsp-servlet-session-listener-tutorial-example-in-eclipse-tomcat/) |
| 5 | [XJavaEEFacesWeb](XJavaEEFacesWeb/) | [netbeans.org](https://netbeans.org/kb/docs/javaee/javaee-gettingstarted_ru.html) |
| 6 | [XConsultingAgencyWeb](XConsultingAgencyWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/jsf20-crud_ru.html) |
| 7 | [XCdiDemoWeb](XCdiDemoWeb/) | [netbeans.org](https://netbeans.org/kb/docs/javaee/cdi-intro_ru.html) |
| 8 | [XHelloSpringWeb](XHelloSpringWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps-spring_ru.html) |
| 9 | [XSpringIdolWeb](XSpringIdolWeb/) | См. презентацию "Spring в действии 2" или книгу Walls C. "Spring In Action" |
| 10 | [XSpringBootHelloWorldWeb](XSpringBootHelloWorldWeb/) | [mkyong.com](http://www.mkyong.com/spring-boot/spring-boot-hello-world-example-jsp/) |
| 11 | [XMvcForBeginnersWeb](XMvcForBeginnersWeb/) | [javastudy.ru](http://javastudy.ru/spring-mvc/spring-mvc-4-example-for-beginners/) |
| 12 | [XHibernateQuickStartWeb](XHibernateQuickStartWeb/) | [javastudy.ru](http://javastudy.ru/hibernate/hibernate-quick-start/) |
| 13 | [XServingMobileWeb](XServingMobileWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/serving-mobile-web-content/) |
| 14 | [XQrCodeWeb](XQrCodeWeb/) | [viralpatel.net](http://viralpatel.net/blogs/create-qr-codes-java-servlet-qr-code-java/), source: [github.com](https://github.com/viralpatel/viralpatel-net-tutorials/blob/master/archive/QR_Code_Servlet.zip) |
| 15 | [XAccessingDataRestWeb](XAccessingDataRestWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/accessing-data-rest/) |
| 16 | [XRelationalDataAccessWeb](XRelationalDataAccessWeb/) | [spring.io](https://spring.io/guides/gs/relational-data-access/) |
| 17 | [XConsumingRestJqueryWeb](XConsumingRestJqueryWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/consuming-rest-jquery/) |
| 18 | [XSpringSecurityWeb](XSpringSecurityWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/securing-web/) |
| 19 | [XValidatingFormInputWeb](XValidatingFormInputWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/validating-form-input/) |
| 20 | [XAuthenticationLDAPWeb](XAuthenticationLDAPWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/authenticating-ldap/) |
| 21 | [XSchedulingTasksWeb](XSchedulingTasksWeb/) | [spring-projects.ru](http://spring-projects.ru/guides/scheduling-tasks/) |
| 22 | [XSpringBootSecurityJpaO7Web](XSpringBootSecurityJpaO7Web/) | [o7planning.org](https://o7planning.org/en/11705/create-a-login-application-with-spring-boot-spring-security-jpa) |
| 23 | [XJavaRushNotebookWeb](XJavaRushNotebookWeb/) | [javarush.ru](https://javarush.ru/groups/posts/497-zavoevanie-spring-boot) |
| 24 | [XSpringBootCrudRestfulServerO7Web](XSpringBootCrudRestfulServerO7Web/) | [o7planning.org](https://o7planning.org/en/11645/crud-restful-web-service-with-spring-boot-example) |
| 25 | [XSpringBootCrudRestfulClientO7Web](XSpringBootCrudRestfulClientO7Web/) | [o7planning.org](https://o7planning.org/en/11647/spring-boot-restful-client-with-resttemplate-example) |