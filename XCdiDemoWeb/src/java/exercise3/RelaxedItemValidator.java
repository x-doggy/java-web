package exercise3;

import exercise2.Item;
import javax.enterprise.inject.Alternative;

/**
 *
 * @author nbuser
 */
@Alternative
public class RelaxedItemValidator implements ItemValidator {

    @Override
    public boolean isValid(Item item) {
        return item.getValue() < (item.getLimit() * 2);
    }
}