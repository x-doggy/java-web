package exercise3;

import exercise2.Item;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;

/**
 *
 * @author nbuser
 */
@RequestScoped
public class FileErrorReporter implements ItemErrorHandler {

    public void eventFired(@Observes Item item) {
        handleItem(item);
    }

    @PostConstruct
    public void init() {
        System.out.println("Creating file error reporter");
    }

    @PreDestroy
    public void release() {
        System.out.println("Closing file error reporter");
    }

    @Override
    public void handleItem(Item item) {
        System.out.println("Saving " + item + " to file");
    }
}