package exercise2;

import java.util.List;

/**
 *
 * @author nbuser
 */
public interface ItemDao {

    List<Item> fetchItems();

}