<%-- 
    Document   : success
    Created on : Oct 22, 2013, 10:50:04 PM
    Author     : davidsmith
--%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login Success</title>
        <link rel="stylesheet" href="stylesheet.css">
    </head>
    <body>
        <h1>Congratulations!</h1>
        <p>You have successfully logged in.</p>
        <p>Your name is: <bean:write name="LoginForm" property="name" /></p>
        <p>Your email address is: <bean:write name="LoginForm" property="email" /></p>
    </body>
</html>
