<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Index | Servlet Session Listener example</title>
	<style>
		.header-group {
			display: flex;
			flex-direction: row;
			justify-content: space-between;
		}

		.form__label {
			font-size: 110%;
			font-weight: bold;
			padding-top: 3em;
		}
	</style>
</head>

<body class="body body__type_index">

	<hgroup class="header-group">
		<h2 class="title title__default">Add User Screen</h2>
		<h4 class="title title__destroy">
			<a href="DestroySession.jsp" class="link link__type_destroy">Destroy this session</a>
		</h4>
	</hgroup>

	<form method="POST" action="AddUser.jsp" class="form">
		<p class="simple">
			<label for="input__user" class="form__label">Enter Username to Add in List:</label>
		</p>
		<input name="user" autofocus id="input__user">
		<input type="submit" value="Add User" class="form__submit">
	</form>

	<ul class="session-list">
	<%
		List<String> users = (List<String>) session.getAttribute("users");
		for(int i = 0; null != users && i < users.size(); i++) {
			out.println("<li class=\"session-list__item\">" + users.get(i));
		}
	%>
	</ul>

</body>
</html>