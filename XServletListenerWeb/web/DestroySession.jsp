<%
	session.invalidate();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Destroy Session | Servlet Session Listener example</title>
</head>

	<body class="body body__type_index">
		<h2 class="title title__default">Session Destroyed successfully...</h2>
		<p class="simple">
			<a href="index.jsp" class="link">Click here to go Back</a>
		</p>
	</body>
</html>