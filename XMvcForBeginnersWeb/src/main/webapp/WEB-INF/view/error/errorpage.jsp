<!DOCTYPE html>
<html lang="en">

  <%@ page language="java" contentType="text/html;charset=UTF-8" %>

  <head>
    <meta charset="utf-8">
    <title>Error page | XMvcForBeginnersWeb</title>
    <style>
      :root,
      .flex-body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .flex-body {
        display: flex;
        flex-wrap: nowrap;
      }
      .flex-text {
        margin: auto;
        padding: 1em;
        border: 3px solid #f00;
        background-color: darksalmon;
        font-size: 1.5em;
      }
    </style>
  </head>
  <body class="flex-body">
    <div class="flex-text">
      <div><b>Incorrect page URL:</b></div>
      <div>${pageContext.request.requestURL}</div>
    </div>
  </body>
</html>
