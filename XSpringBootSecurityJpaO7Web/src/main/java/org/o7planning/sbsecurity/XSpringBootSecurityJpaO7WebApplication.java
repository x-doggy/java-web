package org.o7planning.sbsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XSpringBootSecurityJpaO7WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(XSpringBootSecurityJpaO7WebApplication.class, args);
	}
}
