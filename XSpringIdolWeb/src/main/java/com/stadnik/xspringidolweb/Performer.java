package com.stadnik.xspringidolweb;

public interface Performer {
    void perform();
}
