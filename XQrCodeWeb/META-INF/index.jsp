<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>QR Code in Java Servlet - viralpatel.net</title>
    <style>
      h2 {
        color: white;
        background-color: #3275A8;
        margin:0px;
        padding: 5px;
        height: 40px;
        padding: 15px;
      }

      input {
        font-size: 15px;
      }
      .style1 {
        border: 3px solid #ffaa00;
        font-size: 20px;
      }

      .style2 {
        border: 2px solid #aaff77;
        font-size: 18px;
      }

    </style>
  </head>

  <body>

    <div id="container">
      <header>
        <h2>
          <a href="http://viralpatel.net"><img src="logo.gif"></a>
          Create QR Code in Java Servlet
        </h2>
      </header>
      <main>

        <form action="qrservlet" method="get">
          <p>Enter Text to create QR Code</p>
          <input name="qrtext">
          <input type="submit" value="Generate QR Code">
        </form>

      </main>
      <footer>
        Copyright &copy; <a href="http://viralpatel.net">viralpatel.net</a>
      </footer>

  </body>
</html>
