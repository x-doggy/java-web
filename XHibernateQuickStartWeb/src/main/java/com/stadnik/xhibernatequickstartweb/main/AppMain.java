package com.stadnik.xhibernatequickstartweb.main;

import java.util.Date;
import org.hibernate.Session;
import com.stadnik.xhibernatequickstartweb.dao.ContactEntity;
import com.stadnik.xhibernatequickstartweb.utils.HibernateSessionFactory;
import java.util.Random;

public class AppMain {
  
  private static String genRandomString() {
    String alphabet = "ab1c2d3e4fg5hi6jkl7mno8pqrstu9vwxyz";
    int alphalength = alphabet.length();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 9; i++) {
      sb.append(alphabet.charAt(new Random().nextInt(alphalength)));
    }
    return sb.toString();
  }

  public static void main(String[] args) {
    System.out.println("Hibernate tutorial");

    try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
      session.beginTransaction();
      
      ContactEntity contactEntity = new ContactEntity();
      
      contactEntity.setBirthDate(new Date());
      contactEntity.setFirstName(genRandomString());
      contactEntity.setLastName(genRandomString());
      
      session.save(contactEntity);
      session.getTransaction().commit();
    }

  }
}
