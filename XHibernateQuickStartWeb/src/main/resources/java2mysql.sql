SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `java2mysql`;
CREATE DATABASE `java2mysql` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci */;
USE `java2mysql`;

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(40) COLLATE utf8_general_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_CONTACT_1` (`first_name`,`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSETutf8 COLLATE=utf8_general_ci;

INSERT INTO `contact` (`ID`, `first_name`, `last_name`, `birth_date`, `version`) VALUES
(1,	'Vladimir',	'Stadnik',	'1995-02-22',	1);

DROP TABLE IF EXISTS `contact_hobby_detail`;
CREATE TABLE `contact_hobby_detail` (
  `contact_id` int(11) NOT NULL,
  `hobby_id` varchar(20) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`contact_id`,`hobby_id`),
  KEY `FK_CONTACT_HOBBY_DETAIL_2` (`hobby_id`),
  CONSTRAINT `FK_CONTACT_HOBBY_DETAIL_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_CONTACT_HOBBY_DETAIL_2` FOREIGN KEY (`hobby_id`) REFERENCES `hobby` (`hobby_id`)
) ENGINE=InnoDB DEFAULT CHARSETutf8 COLLATE=utf8_general_ci;

INSERT INTO `contact_hobby_detail` (`contact_id`, `hobby_id`) VALUES
(1,	'1'),
(2,	'1');

DROP TABLE IF EXISTS `contact_tel_detail`;
CREATE TABLE `contact_tel_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `tel_type` varchar(20) COLLATE utf8_general_ci NOT NULL,
  `tel_number` varchar(20) COLLATE utf8_general_ci NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_contact_tel_detail_1` (`contact_id`,`tel_type`),
  CONSTRAINT `FK_CONTACT_TEL_DETAIL_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSETutf8 COLLATE=utf8_general_ci;

INSERT INTO `contact_tel_detail` (`id`, `contact_id`, `tel_type`, `tel_number`, `version`) VALUES
(1,	2,	'1',	'555-31-01',	1),
(2,	1,	'1',	'555-13-22',	0);

-- 2017-11-28 20:04:44
